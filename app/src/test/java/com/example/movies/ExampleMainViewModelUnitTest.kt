package com.example.movies

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.movies.model.api.ApiService
import com.example.movies.model.db.FavoriteMovieDao
import com.example.movies.ui.Constants
import com.example.movies.ui.main.MainViewModel
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.net.SocketException


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(JUnit4::class)
class ExampleMainViewModelUnitTest {
    @Rule @JvmField
    var instantExecutorRule: InstantTaskExecutorRule = InstantTaskExecutorRule()
    @Mock
    lateinit var apiService: ApiService
    @Mock
    lateinit var favoriteMovieDao: FavoriteMovieDao
    @Mock
    lateinit var errorObserver: Observer<Int>

    private lateinit var viewModel: MainViewModel

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        viewModel = MainViewModel(apiService, favoriteMovieDao)
        viewModel.error.observeForever(errorObserver)
    }

    @Test
    fun loadNowPlaying_shouldPostError() {
        runBlocking {
            Mockito.`when`(apiService.nowPlaying(Constants.API_KEY, 1)).thenAnswer { throw SocketException() }
            viewModel.fetchMovies()
            Mockito.verify(errorObserver).onChanged(R.string.error_network_operation)
        }
    }
}