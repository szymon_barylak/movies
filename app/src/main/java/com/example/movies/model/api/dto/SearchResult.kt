package com.example.movies.model.api.dto

import com.google.gson.annotations.SerializedName

data class SearchResult(
    @SerializedName("page") val page: Int,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("results") val movies: List<Movie>
)