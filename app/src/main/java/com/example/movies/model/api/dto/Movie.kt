package com.example.movies.model.api.dto

import com.google.gson.annotations.SerializedName
import java.util.*

data class Movie(
    @SerializedName("id") val id: Int,
    @SerializedName("title") val title: String,
    @SerializedName("backdrop_path") val imageBackdrop: String?,
    @SerializedName("poster_path") val imagePoster: String?,
    @SerializedName("release_date") val releaseDate: Date?,
    @SerializedName("vote_average") val vote: Double,
    var isFavorite: Boolean = false
) {
    companion object {
        fun empty(id: Int): Movie {
            return Movie(id, "", "", "", Date(), 0.0, false)
        }
    }


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Movie

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id
    }
}