package com.example.movies.model.api.dto

import com.google.gson.annotations.SerializedName
import java.util.*

data class MovieDetails(
    @SerializedName("id") val id: Int,
    @SerializedName("backdrop_path") val imageBackground: String?,
    @SerializedName("poster_path") val imageMain: String?,
    @SerializedName("overview") val overview: String,
    @SerializedName("release_date") val releaseDate: Date?,
    @SerializedName("title") val title: String,
    @SerializedName("vote_average") val voteAverage: Double,
    @SerializedName("vote_count") val voteCount: Int,
    var isFavorite: Boolean = false
)