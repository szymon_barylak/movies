package com.example.movies.model.api

import com.example.movies.model.api.dto.MovieDetails
import com.example.movies.model.api.dto.NowPlaying
import com.example.movies.model.api.dto.SearchResult
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("movie/now_playing/")
    suspend fun nowPlaying(@Query("api_key") apiKey: String, @Query("page") page: Int): NowPlaying

    @GET("movie/{movieId}")
    suspend fun details(@Path("movieId") movieId: Int, @Query("api_key") apiKey: String): MovieDetails

    @GET("search/movie")
    suspend fun serach(@Query("api_key") apiKey: String, @Query("query") query: String, @Query("page") page: Int): SearchResult
}