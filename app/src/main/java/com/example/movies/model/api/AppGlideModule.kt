package com.example.movies.model.api

import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.Excludes
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.integration.okhttp3.OkHttpLibraryGlideModule
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader
import com.bumptech.glide.load.model.GlideUrl
import com.example.movies.ui.App
import okhttp3.OkHttpClient
import java.io.InputStream
import javax.inject.Inject

@GlideModule
@Excludes(OkHttpLibraryGlideModule::class)
class AppGlideModule: com.bumptech.glide.module.AppGlideModule() {
    @Inject
    lateinit var okHttpClient: OkHttpClient
    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        super.registerComponents(context, glide, registry)
        (context.applicationContext as App).androidInjector().inject(this)
        val factory = OkHttpUrlLoader.Factory(okHttpClient)
        registry.replace(GlideUrl::class.java, InputStream::class.java, factory)
    }
}