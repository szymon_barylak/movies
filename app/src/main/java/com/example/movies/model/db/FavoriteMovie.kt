package com.example.movies.model.db

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity
class FavoriteMovie() {
    @PrimaryKey
    var movieId: Int = 0

    @Ignore
    constructor(id: Int) : this() {
        movieId = id
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FavoriteMovie

        if (movieId != other.movieId) return false

        return true
    }

    override fun hashCode(): Int {
        return movieId
    }


}