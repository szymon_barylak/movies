package com.example.movies.model.api

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.movies.model.db.FavoriteMovie
import com.example.movies.model.db.FavoriteMovieDao

@Database(
    entities = [FavoriteMovie::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): FavoriteMovieDao
}