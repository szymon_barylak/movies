package com.example.movies.model.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
abstract class FavoriteMovieDao {
    @Insert
    abstract suspend fun insert(movie: FavoriteMovie)

    @Delete
    abstract suspend fun delete(movie: FavoriteMovie)

    @Query("SELECT * FROM FavoriteMovie")
    abstract suspend fun all(): MutableList<FavoriteMovie>
}