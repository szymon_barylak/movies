package com.example.movies.di.scopes

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import javax.inject.Scope

@Suppress("unused")
@Scope
@Retention(RetentionPolicy.RUNTIME)
annotation class PerFragment