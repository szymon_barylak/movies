package com.example.movies.di

import android.app.Application
import com.example.movies.di.modules.*
import com.example.movies.di.scopes.PerApp
import com.example.movies.ui.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule

@PerApp
@Component(modules = [AndroidInjectionModule::class, AndroidSupportInjectionModule::class,
    AppModule::class, ApiModule::class, DatabaseModule::class, ActivityBuilder::class, ViewModelModule::class, GlideModule::class])
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    fun inject(app: App)
}