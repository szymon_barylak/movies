package com.example.movies.di.modules

import android.app.Application
import android.content.Context
import com.example.movies.di.scopes.PerApp
import dagger.Module
import dagger.Provides

@Module
class AppModule {
    @Provides
    @PerApp
    fun provideContext(application: Application): Context {
        return application
    }
}