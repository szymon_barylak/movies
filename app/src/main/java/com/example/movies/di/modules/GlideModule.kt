package com.example.movies.di.modules

import com.example.movies.model.api.AppGlideModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class GlideModule {
    @ContributesAndroidInjector
    abstract fun provideAppGlideModule(): AppGlideModule
}