package com.example.movies.di.modules

import android.content.Context
import androidx.room.Room
import com.example.movies.di.scopes.PerApp
import com.example.movies.model.api.AppDatabase
import com.example.movies.model.db.FavoriteMovieDao
import dagger.Module
import dagger.Provides

@Module
class DatabaseModule {
    companion object {
        private const val DATABASE_NAME = "movies_db"
    }

    @Provides
    @PerApp
    fun provideMovieDao(appDatabase: AppDatabase): FavoriteMovieDao {
        return appDatabase.movieDao()
    }

    @Provides
    @PerApp
    fun provideAppDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
            .build()
    }
}