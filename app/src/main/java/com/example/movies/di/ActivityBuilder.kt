@file:Suppress("unused")

package com.example.movies.di

import com.example.movies.di.scopes.PerActivity
import com.example.movies.ui.details.MovieDetailsActivity
import com.example.movies.ui.details.di.MovieDetailsModule
import com.example.movies.ui.main.MainActivity
import com.example.movies.ui.main.di.MainActivityModule
import com.example.movies.ui.search.SearchActivity
import com.example.movies.ui.search.di.SearchActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
    @PerActivity
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun bindMainActivity(): MainActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [MovieDetailsModule::class])
    abstract fun bindMovieDetailsActivity(): MovieDetailsActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [SearchActivityModule::class])
    abstract fun bindSearchDetailsActivity(): SearchActivity

}