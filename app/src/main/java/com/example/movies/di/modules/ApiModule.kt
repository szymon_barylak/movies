package com.example.movies.di.modules

import android.os.Build
import android.util.Log
import com.example.movies.di.scopes.PerApp
import com.example.movies.model.api.ApiService
import com.example.movies.model.api.Tls12SocketFactory
import com.example.movies.ui.Constants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import dagger.Module
import dagger.Provides
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.TlsVersion
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext


@Module
class ApiModule {
    private fun enableTls12OnPreLollipop(client: OkHttpClient.Builder): OkHttpClient.Builder {
        if (Build.VERSION.SDK_INT in 19..21) {
            try {
                val sc: SSLContext = SSLContext.getInstance("TLSv1.2")
                sc.init(null, null, null)
                client.sslSocketFactory(Tls12SocketFactory(sc.socketFactory))
                val cs = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                    .tlsVersions(TlsVersion.TLS_1_2).build()
                val specs: MutableList<ConnectionSpec> = ArrayList()
                specs.add(cs)
                specs.add(ConnectionSpec.COMPATIBLE_TLS)
                specs.add(ConnectionSpec.CLEARTEXT)
                client.connectionSpecs(specs)
            } catch (exc: Exception) {
                Log.e("OkHttpTLSCompat", "Error while setting TLS 1.2", exc)
            }
        }
        return client
    }

    @Provides
    @PerApp
    fun provideGson(): Gson {
        return GsonBuilder()
            .serializeNulls()
            .registerTypeAdapter(Date::class.java, JsonDeserializer { json, _, _ ->
                val sdf = SimpleDateFormat(Constants.DATE_FORMAT, Locale.getDefault())
                val jsonDate = json.asString

                if (jsonDate.isNotEmpty())
                    return@JsonDeserializer sdf.parse(jsonDate)
                else
                    return@JsonDeserializer null
            })
            .create()
    }

    @Provides
    @PerApp
    fun provideHttpClient(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
            .retryOnConnectionFailure(true)
            .connectTimeout(5, TimeUnit.SECONDS)
            .writeTimeout(5, TimeUnit.SECONDS)
            .readTimeout(5, TimeUnit.SECONDS)

        return enableTls12OnPreLollipop(httpClient).build()
    }

    @Provides
    @PerApp
    fun provideRetrofit(client: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(Constants.ENDPOINT_URL)
            .build()
    }

    @Provides
    @PerApp
    fun provideApi(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }
}