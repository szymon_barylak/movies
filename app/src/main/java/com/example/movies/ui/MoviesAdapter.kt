package com.example.movies.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.movies.R
import com.example.movies.model.LoadingItem
import com.example.movies.model.api.dto.Movie
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MoviesAdapter(private val listener: MovieItemListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val VIEW_TYPE_ITEM = 0
        private const val VIEW_TYPE_LOADING = 1
    }
    private val items: MutableList<Any> = ArrayList()
    private val sdf = SimpleDateFormat("yyyy", Locale.getDefault())

    private class LoadingViewHolder(v: View) : RecyclerView.ViewHolder(v)
    private class ItemViewHolder(v: View, listener: MovieItemListener) : RecyclerView.ViewHolder(v) {
        val tvTitle: TextView = v.findViewById(R.id.tvMovieItemTitle)
        val tvYear: TextView = v.findViewById(R.id.tvMovieItemYear)
        val ivPhoto: ImageView = v.findViewById(R.id.ivMovieItemPhoto)
        val ivFavorite: ImageView = v.findViewById(R.id.ivMovieItemFavorite)

        init {
            v.setOnClickListener { listener.onItemClick(adapterPosition) }
            ivFavorite.setOnClickListener { listener.changeFavoriteStatus(adapterPosition) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return when(viewType) {
            VIEW_TYPE_ITEM -> ItemViewHolder(inflater.inflate(R.layout.list_item_movie, parent, false), listener)
            else -> LoadingViewHolder(inflater.inflate(R.layout.list_item_loading, parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is ItemViewHolder) {
            val item = items[position] as Movie
            holder.tvTitle.text = item.title
            item.releaseDate?.let { holder.tvYear.text = sdf.format(it) }

            val favoriteDrawable = if(item.isFavorite) {
                VectorDrawableCompat.create(holder.ivFavorite.resources, R.drawable.ic_round_favorite_24, null)
            } else {
                VectorDrawableCompat.create(holder.ivFavorite.resources, R.drawable.ic_round_favorite_border_24, null)
            }

            holder.ivFavorite.setImageDrawable(favoriteDrawable)

            item.imagePoster?.let {
                Glide.with(holder.ivPhoto)
                    .load(Constants.IMAGE_URL + it)
                    .transform(RoundedCorners(30))
                    .error(
                        VectorDrawableCompat.create(
                            holder.ivPhoto.resources,
                            R.drawable.ic_no_movie_image,
                            null
                        )
                    )
                    .into(holder.ivPhoto)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when(items[position]) {
            is Movie -> VIEW_TYPE_ITEM
            else -> VIEW_TYPE_LOADING
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun addMovies(movies: List<Movie>) {
        if(items.size > 0)
            removeLoadingItem()

        val startPosition = items.size
        items.addAll(movies)
        notifyItemRangeInserted(startPosition, movies.size)
    }

    fun addLoadingItem() {
        if(!items.contains(LoadingItem)) {
            items.add(LoadingItem)
            notifyItemInserted(items.size - 1)
        }
    }

    fun removeLoadingItem() {
        if(items.contains(LoadingItem)) {
            items.remove(LoadingItem)
            notifyItemRemoved(items.size)
        }
    }

    fun changeFavoriteStatus(movie: Movie) {
        val index = items.indexOf(movie)
        val item = items[index] as Movie
        item.isFavorite = !item.isFavorite
        notifyItemChanged(index)
    }

    fun getItem(position: Int): Movie {
        return items[position] as Movie
    }

    fun setMovies(movies: List<Movie>) {
        items.clear()
        items.addAll(movies)
        notifyDataSetChanged()
    }

    fun updateMovies(movies: List<Movie>) {
        movies.forEach { movie ->
            val index = items.indexOf(movie)
            if(index > -1) {
                (items[index] as Movie).isFavorite = !(items[index] as Movie).isFavorite
                notifyDataSetChanged()
            }
        }
    }
}