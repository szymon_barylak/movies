package com.example.movies.ui.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.movies.model.api.ApiService
import com.example.movies.model.api.dto.MovieDetails
import com.example.movies.model.db.FavoriteMovie
import com.example.movies.model.db.FavoriteMovieDao
import com.example.movies.ui.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MovieDetailsViewModel() : ViewModel() {
    private lateinit var favoriteMovieDao: FavoriteMovieDao
    private lateinit var apiService: ApiService
    var movieId: Int = -1
    private val favoriteMovies: MutableList<FavoriteMovie> = ArrayList()
    var isLoading: MutableLiveData<Boolean> = MutableLiveData(true)
    var favoriteChanged: MutableLiveData<Boolean> = MutableLiveData(false)
    var error: MutableLiveData<String> = MutableLiveData("")
    val movie: MutableLiveData<MovieDetails> = MutableLiveData()

    @Inject
    constructor(apiService: ApiService, favoriteMovieDao: FavoriteMovieDao): this() {
        this.apiService = apiService
        this.favoriteMovieDao = favoriteMovieDao

        viewModelScope.launch(Dispatchers.IO) {
            favoriteMovies.addAll(favoriteMovieDao.all())
        }
    }

    fun loadMovieDetails() {
        isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val fetchedMovie = apiService.details(movieId, Constants.API_KEY)
                mapFavorite(fetchedMovie)

                withContext(Dispatchers.Main) {
                    movie.postValue(fetchedMovie)
                    favoriteChanged.postValue(fetchedMovie.isFavorite)
                }
            } catch (ex: Exception) {
                error.postValue(ex.message)
            }

            withContext(Dispatchers.Main) {
                isLoading.postValue(false)
            }
        }
    }

    private fun mapFavorite(movie: MovieDetails) {
        if(favoriteMovies.contains(FavoriteMovie(movie.id)))
            movie.isFavorite = true
    }

    fun favoriteMovie() {
        movie.value?.let {
            viewModelScope.launch(Dispatchers.IO) {
                if (it.isFavorite) {
                    favoriteMovieDao.delete(FavoriteMovie(it.id))
                } else {
                    favoriteMovieDao.insert(FavoriteMovie(it.id))
                }

                it.isFavorite = !it.isFavorite
                withContext(Dispatchers.Main) {
                    favoriteChanged.postValue(it.isFavorite)
                }
            }
        }
    }
}