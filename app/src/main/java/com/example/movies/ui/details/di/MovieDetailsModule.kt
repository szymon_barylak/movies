package com.example.movies.ui.details.di

import com.example.movies.di.scopes.PerActivity
import dagger.Module
import dagger.Provides
import java.text.SimpleDateFormat
import java.util.*

@Module
class MovieDetailsModule {
    @Provides
    @PerActivity
    fun provideSimpleDateFormat(): SimpleDateFormat {
        return SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
    }

}