package com.example.movies.ui.search

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movies.R
import com.example.movies.di.ViewModelFactory
import com.example.movies.model.api.dto.Movie
import com.example.movies.ui.MovieItemListener
import com.example.movies.ui.MoviesAdapter
import com.example.movies.ui.details.MovieDetailsActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

class SearchActivity : AppCompatActivity(), MovieItemListener {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var adapter: MoviesAdapter
    val model: SearchViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        val watcher = object : TextWatcher {
            private var searchFor = ""

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val searchText = s.toString().trim()
                if (searchText == searchFor)
                    return

                searchFor = searchText

                lifecycleScope.launch {
                    delay(300)  //debounce timeOut
                    if (searchText != searchFor || searchFor.isEmpty())
                        return@launch

                    model.searchNewQuery(searchFor)
                }
            }

            override fun afterTextChanged(s: Editable?) = Unit
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
        }

        rvSearchItems.layoutManager = LinearLayoutManager(applicationContext)
        rvSearchItems.setHasFixedSize(true)
        rvSearchItems.adapter = adapter
        initScrollListener()
        etSearch.addTextChangedListener(watcher)

        observers()
    }

    override fun onResume() {
        super.onResume()
        model.refreshFavorites()
    }

    private fun initScrollListener() {
        rvSearchItems.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager

                if (!model.isNextPageLoading && !model.isOnLastPage()) {
                    if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == adapter.itemCount - 1) {
                        rvSearchItems.post {
                            adapter.addLoadingItem()
                            model.searchNextPage()
                        }
                    }
                }
            }
        })
    }

    private fun observers() {
        model.movies.observe(this, {
            if (model.currentPage == 1) {
                adapter.setMovies(it.movies)

                if(it.movies.isEmpty()) {
                    tvSearchErrorText.setText(R.string.overall_no_results)
                    gSearchError.visibility = View.VISIBLE
                    gSearchContent.visibility = View.GONE
                    return@observe
                }

                gSearchError.visibility = View.GONE
                gSearchContent.visibility = View.VISIBLE
            }
            else {
                adapter.addMovies(it.movies)
            }
        })
        model.error.observe(this, { onError(it) })
        model.favoriteChanged.observe(this, { onFavoriteChanged(it) })
        model.updatedMovies.observe(this, { adapter.updateMovies(it) })
    }

    private fun onFavoriteChanged(movie: Movie) {
        adapter.changeFavoriteStatus(movie)
    }

    private fun onError(message: Int) {
        if(message != -1) {
            adapter.removeLoadingItem()
            if(adapter.itemCount == 0) {
                tvSearchErrorText.setText(message)
                gSearchError.visibility = View.VISIBLE
                gSearchContent.visibility = View.GONE
            }
            else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun changeFavoriteStatus(position: Int) {
        val movie = adapter.getItem(position)
        if(movie.isFavorite) {
            model.removeFromFavorites(movie)
        } else {
            model.addToFavorites(movie)
        }
    }

    override fun onItemClick(position: Int) {
        val movie = adapter.getItem(position)
        val intent = Intent(this, MovieDetailsActivity::class.java).apply {
            putExtra(MovieDetailsActivity.EXTRA_MOVIE_ID, movie.id)
        }
        startActivity(intent)
    }

}