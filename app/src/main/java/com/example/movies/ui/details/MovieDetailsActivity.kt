package com.example.movies.ui.details

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.movies.R
import com.example.movies.di.ViewModelFactory
import com.example.movies.model.api.dto.MovieDetails
import com.example.movies.ui.Constants
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_movie_details.*
import java.text.SimpleDateFormat
import javax.inject.Inject

class MovieDetailsActivity : AppCompatActivity() {
    companion object {
        const val EXTRA_MOVIE_ID = "EXTRA_MOVIE_ID"
        const val RETURN_CHANGED_MOVIE_ID = "RETURN_CHANGED_MOVIE_ID"
        const val RETURN_CHANGED_MOVIE_FAVORITE = "RETURN_CHANGED_MOVIE_FAVORITE"
    }
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var sdf: SimpleDateFormat
    val model: MovieDetailsViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)
        model.movieId = intent.getIntExtra(EXTRA_MOVIE_ID, -1)
        fabDetailsFavorite.setOnClickListener { onFavoriteClick() }
        bDetailsRetry.setOnClickListener { onRetryClick() }

        if(model.movieId == -1) {
            Toast.makeText(this, R.string.error_wrong_movie_id, Toast.LENGTH_LONG).show()
            finish()
        }

        observers()
        model.loadMovieDetails()
    }

    private fun onRetryClick() {
        gDetailsLoading.visibility = View.VISIBLE
        gDetailsContent.visibility = View.GONE
        gDetailsError.visibility = View.GONE
        model.loadMovieDetails()
    }

    private fun onFavoriteClick() {
        model.favoriteMovie()
    }

    @SuppressLint("SetTextI18n")
    private fun observers() {
        model.movie.observe(this, { fillContent(it) })
        model.isLoading.observe(this, { onLoading(it) })
        model.error.observe(this, { onError(it) })
        model.favoriteChanged.observe(this, { onFavoriteChanged(it) })
    }

    private fun onFavoriteChanged(it: Boolean) {
        val image = if(it)
            VectorDrawableCompat.create(resources, R.drawable.ic_round_favorite_24, null)
         else
            VectorDrawableCompat.create(resources, R.drawable.ic_round_favorite_border_24, null)
        fabDetailsFavorite.setImageDrawable(image)

        val intent = Intent().apply {
            putExtra(RETURN_CHANGED_MOVIE_ID, model.movieId)
            putExtra(RETURN_CHANGED_MOVIE_FAVORITE, it)
        }
        setResult(RESULT_OK, intent)
    }

    @SuppressLint("SetTextI18n")
    private fun fillContent(movie: MovieDetails) {
        gDetailsContent.visibility = View.VISIBLE
        gDetailsLoading.visibility = View.GONE
        gDetailsError.visibility = View.GONE

        title = movie.title
        tvDetailsTitle.text = movie.title
        tvDetailsOverview.text = movie.overview
        movie.releaseDate?.let { tvDetailsDate.text = sdf.format(it) }
        tvDetailsRate.text = "%.1f / %d reviews".format(movie.voteAverage, movie.voteCount)

        Glide.with(this)
            .load(Constants.IMAGE_URL + movie.imageBackground)
            .centerCrop()
            .error(VectorDrawableCompat.create(resources, R.drawable.ic_no_movie_image, null))
            .into(ivDetailsPhotoBackground)

        Glide.with(this)
            .load(Constants.IMAGE_URL + movie.imageMain)
            .transform(RoundedCorners(50))
            .error(VectorDrawableCompat.create(resources, R.drawable.ic_no_movie_image, null))
            .into(ivDetailsPhotoMain)
    }

    private fun onLoading(isLoading: Boolean) {
        if(isLoading) {
            gDetailsLoading.visibility = View.VISIBLE
            gDetailsError.visibility = View.GONE
            gDetailsContent.visibility = View.GONE
        } else {
            gDetailsLoading.visibility = View.GONE
        }
    }

    private fun onError(message: String?) {
        if(message.isNullOrEmpty()) {
            gDetailsError.visibility = View.GONE
        } else {
            tvDetailsErrorText.text = message
            gDetailsError.visibility = View.VISIBLE
            gDetailsContent.visibility = View.GONE
            gDetailsLoading.visibility = View.GONE
        }
    }
}