package com.example.movies.ui

class Constants {
    companion object {
        const val ENDPOINT_URL = "https://api.themoviedb.org/3/"
        const val IMAGE_URL = "https://image.tmdb.org/t/p/w500"
        const val API_KEY = "2bc195e31463e889fde169fbfac76c52"
        const val DATE_FORMAT = "yyyy-MM-dd"

    }
}