package com.example.movies.ui.search.di

import com.example.movies.di.scopes.PerActivity
import com.example.movies.ui.MovieItemListener
import com.example.movies.ui.MoviesAdapter
import com.example.movies.ui.search.SearchActivity
import dagger.Module
import dagger.Provides

@Module
class SearchActivityModule {
    @Provides
    @PerActivity
    fun provideAdapter(listener: MovieItemListener): MoviesAdapter {
        return MoviesAdapter(listener)
    }

    @Provides
    @PerActivity
    fun provideMovieItemListener(activity: SearchActivity): MovieItemListener {
        return activity
    }
}