@file:Suppress("MemberVisibilityCanBePrivate")

package com.example.movies.ui.main

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.movies.R
import com.example.movies.model.api.ApiService
import com.example.movies.model.api.dto.Movie
import com.example.movies.model.api.dto.NowPlaying
import com.example.movies.model.db.FavoriteMovie
import com.example.movies.model.db.FavoriteMovieDao
import com.example.movies.ui.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MainViewModel constructor(): ViewModel() {
    val movies: MutableList<Movie> = ArrayList()
    private lateinit var favoriteMovieDao: FavoriteMovieDao
    private lateinit var apiService: ApiService
    var isNextPageLoading: Boolean = false
    private var currentPage = 1
    private var favoriteMovies: MutableList<FavoriteMovie> = ArrayList()
    val newMovies: MutableLiveData<NowPlaying> = MutableLiveData()
    val updatedMovies: MutableLiveData<List<Movie>> = MutableLiveData()
    var favoriteChanged: MutableLiveData<Movie> = MutableLiveData()
    var error: MutableLiveData<Int> = MutableLiveData()

    @Inject
    constructor(apiService: ApiService, favoriteMovieDao: FavoriteMovieDao): this() {
        this.apiService = apiService
        this.favoriteMovieDao = favoriteMovieDao
    }

    fun addToFavorites(movie: Movie) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                favoriteMovieDao.insert(FavoriteMovie(movie.id))
                favoriteChanged.postValue(movie)
            } catch (ex: Exception) {
                Log.d(MainViewModel::class.java.name, "addToFavorites", ex)
                error.postValue(R.string.error_database)
            }
        }
    }

    fun removeFromFavorites(movie: Movie) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                favoriteMovieDao.delete(FavoriteMovie(movie.id))
                favoriteChanged.postValue(movie)
            } catch (ex: Exception) {
                error.postValue(R.string.error_database)
            }
        }
    }

    fun loadNowPlayingMovies() {
        viewModelScope.launch(Dispatchers.IO) {
            fetchMovies()
        }
    }

    suspend fun fetchMovies() {
        isNextPageLoading = true
        try {
            if(currentPage == 1)
                favoriteMovies = favoriteMovieDao.all()

            val fetchedMovies = apiService.nowPlaying(Constants.API_KEY, currentPage)
            mapFavourites(fetchedMovies.movies)
            movies.addAll(fetchedMovies.movies)

            withContext(Dispatchers.Main) {
                newMovies.postValue(fetchedMovies)
            }
            currentPage++
        } catch (ex: Exception) {
            error.postValue(R.string.error_network_operation)
        }

        isNextPageLoading = false
    }

    private fun mapFavourites(movies: List<Movie>) {
        favoriteMovies.forEach {
            if(movies.contains(Movie.empty(it.movieId))) {
                val index = movies.indexOf(Movie.empty(it.movieId))
                movies[index].isFavorite = true
            }
        }
    }

    fun refreshFavorites() {
        viewModelScope.launch(Dispatchers.IO) {
            favoriteMovies = favoriteMovieDao.all()
            val favChangedMovies = movies.filter {
                (favoriteMovies.contains(FavoriteMovie(it.id)) && !it.isFavorite)
                        || (!favoriteMovies.contains(FavoriteMovie(it.id)) && it.isFavorite)

            }

            withContext(Dispatchers.Main) {
                updatedMovies.postValue(favChangedMovies)
            }
        }
    }

    fun isOnLastPage(): Boolean {
        return currentPage >= (newMovies.value?.totalPages ?: 1)
    }
}
