package com.example.movies.ui.search

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.movies.R
import com.example.movies.model.api.ApiService
import com.example.movies.model.api.dto.Movie
import com.example.movies.model.api.dto.SearchResult
import com.example.movies.model.db.FavoriteMovie
import com.example.movies.model.db.FavoriteMovieDao
import com.example.movies.ui.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SearchViewModel constructor(): ViewModel() {
    private lateinit var favoriteMovieDao: FavoriteMovieDao
    private lateinit var apiService: ApiService
    var isNextPageLoading: Boolean = false
    var currentPage = 1
    private var currentQuery = ""
    private var favoriteMovies: MutableList<FavoriteMovie> = ArrayList()
    val movies: MutableLiveData<SearchResult> = MutableLiveData()
    val updatedMovies: MutableLiveData<List<Movie>> = MutableLiveData()
    var favoriteChanged: MutableLiveData<Movie> = MutableLiveData()
    var error: MutableLiveData<Int> = MutableLiveData(-1)

    @Inject
    constructor(apiService: ApiService, favoriteMovieDao: FavoriteMovieDao): this() {
        this.apiService = apiService
        this.favoriteMovieDao = favoriteMovieDao

        viewModelScope.launch(Dispatchers.IO) {
            favoriteMovies = favoriteMovieDao.all()
        }
    }

    fun addToFavorites(movie: Movie) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                favoriteMovieDao.insert(FavoriteMovie(movie.id))
                favoriteChanged.postValue(movie)
            } catch (ex: Exception) {
                Log.d(SearchViewModel::class.java.name, "addToFavorites", ex)
                error.postValue(R.string.error_database)
            }
        }
    }

    fun removeFromFavorites(movie: Movie) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                favoriteMovieDao.delete(FavoriteMovie(movie.id))
                favoriteChanged.postValue(movie)
            } catch (ex: Exception) {
                Log.d(SearchViewModel::class.java.name, "removeFromFavorites", ex)
                error.postValue(R.string.error_database)
            }
        }
    }

    fun searchNextPage() {
        isNextPageLoading = true
        viewModelScope.launch(Dispatchers.IO) {
            try {
                currentPage++
                val fetchedMovies = apiService.serach(Constants.API_KEY, currentQuery, currentPage)
                mapFavourites(fetchedMovies.movies)

                withContext(Dispatchers.Main) {
                    movies.postValue(fetchedMovies)
                }

            } catch (ex: Exception) {
                Log.d(SearchViewModel::class.java.name, "searchNextPage", ex)
                error.postValue(R.string.error_network_operation)
            }

            isNextPageLoading = false
        }
    }

    fun searchNewQuery(query: String) {
        isNextPageLoading = true
        currentQuery = query
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val fetchedMovies = apiService.serach(Constants.API_KEY, query, 1)
                mapFavourites(fetchedMovies.movies)

                withContext(Dispatchers.Main) {
                    movies.postValue(fetchedMovies)
                }
                currentPage = 1
            } catch (ex: Exception) {
                Log.d(SearchViewModel::class.java.name, "searchNewQuery", ex)
                error.postValue(R.string.error_network_operation)
            }

            isNextPageLoading = false
        }
    }

    private fun mapFavourites(movies: List<Movie>) {
        favoriteMovies.forEach {
            if(movies.contains(Movie.empty(it.movieId))) {
                val index = movies.indexOf(Movie.empty(it.movieId))
                movies[index].isFavorite = true
            }
        }
    }

    fun refreshFavorites() {
        viewModelScope.launch(Dispatchers.IO) {
            favoriteMovies = favoriteMovieDao.all()
            val favChangedMovies = movies.value?.movies?.filter {
                (favoriteMovies.contains(FavoriteMovie(it.id)) && !it.isFavorite)
                        || (!favoriteMovies.contains(FavoriteMovie(it.id)) && it.isFavorite)
            } ?: emptyList()

            withContext(Dispatchers.Main) {
                updatedMovies.postValue(favChangedMovies)
            }
        }
    }

    fun isOnLastPage(): Boolean {
        return currentPage >= (movies.value?.totalPages ?: 1)
    }
}
