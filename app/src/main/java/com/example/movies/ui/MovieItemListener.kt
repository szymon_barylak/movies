package com.example.movies.ui

interface MovieItemListener {
    fun changeFavoriteStatus(position: Int)
    fun onItemClick(position: Int)
}