package com.example.movies.ui.main.di

import com.example.movies.di.scopes.PerActivity
import com.example.movies.ui.MovieItemListener
import com.example.movies.ui.main.MainActivity
import com.example.movies.ui.MoviesAdapter
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {
    @Provides
    @PerActivity
    fun provideAdapter(listener: MovieItemListener): MoviesAdapter {
        return MoviesAdapter(listener)
    }

    @Provides
    @PerActivity
    fun provideMovieItemListener(activity: MainActivity): MovieItemListener {
        return activity
    }
}