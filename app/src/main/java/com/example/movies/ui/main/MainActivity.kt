package com.example.movies.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movies.R
import com.example.movies.di.ViewModelFactory
import com.example.movies.model.api.dto.Movie
import com.example.movies.ui.MovieItemListener
import com.example.movies.ui.MoviesAdapter
import com.example.movies.ui.details.MovieDetailsActivity
import com.example.movies.ui.search.SearchActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity(), MovieItemListener {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var adapter: MoviesAdapter
    val model: MainViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bMainRetry.setOnClickListener { onRetryClick() }
        rvMovies.layoutManager = LinearLayoutManager(applicationContext)
        rvMovies.setHasFixedSize(true)
        rvMovies.adapter = adapter
        initScrollListener()

        observers()
        adapter.setMovies(model.movies)
        model.loadNowPlayingMovies()
    }

    private fun onRetryClick() {
        pbMainLoading.visibility = View.VISIBLE
        model.loadNowPlayingMovies()
    }

    override fun onResume() {
        super.onResume()
        model.refreshFavorites()
    }

    private fun initScrollListener() {
        rvMovies.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager

                if (!model.isNextPageLoading && !model.isOnLastPage()) {
                    if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == adapter.itemCount - 1) {
                        rvMovies.post {
                            adapter.addLoadingItem()
                            model.loadNowPlayingMovies()
                        }
                    }
                }
            }
        })
    }

    private fun observers() {
        model.newMovies.observe(this, {
            gMainError.visibility = View.GONE
            gMainContent.visibility = View.VISIBLE

            adapter.addMovies(it.movies)
        })
        model.error.observe(this, { onError(it) })
        model.favoriteChanged.observe(this, { onFavoriteChanged(it) })
        model.updatedMovies.observe(this, { adapter.updateMovies(it) })
    }

    private fun onFavoriteChanged(movie: Movie) {
        adapter.changeFavoriteStatus(movie)
    }

    private fun onError(message: Int) {
        adapter.removeLoadingItem()

        if(message != -1) {
            if(adapter.itemCount == 0) {
                tvMainErrorText.setText(message)
                gMainError.visibility = View.VISIBLE
                gMainContent.visibility = View.GONE
                pbMainLoading.visibility = View.GONE
            }
            else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun changeFavoriteStatus(position: Int) {
        val movie = adapter.getItem(position)
        if(movie.isFavorite) {
            model.removeFromFavorites(movie)
        } else {
            model.addToFavorites(movie)
        }
    }

    override fun onItemClick(position: Int) {
        val movie = adapter.getItem(position)
        val intent = Intent(this, MovieDetailsActivity::class.java).apply {
            putExtra(MovieDetailsActivity.EXTRA_MOVIE_ID, movie.id)
        }
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_search -> {
                startActivity(Intent(this, SearchActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}